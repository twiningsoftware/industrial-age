﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Bronze.Common;
using Bronze.Contracts;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace IndustrialAge.Events
{
    public class SeasonalBiomeSwapper : ISubSimulator, IDataLoader, IWorldStateService
    {
        public string ElementName => "seasonal_biomes";

        private readonly IWorldManager _worldManager;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IWorldMinimapDrawer _worldMinimapDrawer;

        private List<Point> _districtTraverse;

        private double[,] _noiseMap;
        private Cell[][] _cellsInOrder;
        private Trait _controlFlagTrait;
        private Dictionary<Biome, SeasonMapping<Biome>> _biomeMappingsByFrom;
        private Dictionary<Biome, SeasonMapping<Biome>> _biomeMappingsByTo;
        private Dictionary<ICellFeature, SeasonMapping<ICellFeature>> _cellFeatureMappingsByFrom;
        private Dictionary<ICellFeature, SeasonMapping<ICellFeature>> _cellFeatureMappingsByTo;
        private Dictionary<Terrain, SeasonMapping<Terrain>> _terrainMappingsByFrom;
        private Dictionary<Terrain, SeasonMapping<Terrain>> _terrainMappingsByTo;
        private Dictionary<Biome, double> _adjacentMods;
        private int _nextIndex;
        private int _worldHeight;

        private List<Tuple<double, District>> _districtQueue;
        private int _placeInDistrict;
        
        public SeasonalBiomeSwapper(
            IWorldManager worldManager,
            IXmlReaderUtil xmlReaderUtil,
            IGamedataTracker gamedataTracker,
            IWorldMinimapDrawer worldMinimapDrawer)
        {
            _worldManager = worldManager;
            _xmlReaderUtil = xmlReaderUtil;
            _gamedataTracker = gamedataTracker;
            _worldMinimapDrawer = worldMinimapDrawer;

            _biomeMappingsByFrom = new Dictionary<Biome, SeasonMapping<Biome>>();
            _biomeMappingsByTo = new Dictionary<Biome, SeasonMapping<Biome>>();
            _adjacentMods = _gamedataTracker.Biomes
                .ToDictionary(b => b, b => 0.0);

            _cellFeatureMappingsByFrom = new Dictionary<ICellFeature, SeasonMapping<ICellFeature>>();
            _cellFeatureMappingsByTo = new Dictionary<ICellFeature, SeasonMapping<ICellFeature>>();

            _terrainMappingsByFrom = new Dictionary<Terrain, SeasonMapping<Terrain>>();
            _terrainMappingsByTo = new Dictionary<Terrain, SeasonMapping<Terrain>>();
            
            _districtTraverse = new List<Point>();
            for(var x = 0; x < TilePosition.TILES_PER_CELL; x++)
            {
                for(var y = 0; y < TilePosition.TILES_PER_CELL; y++)
                {
                    _districtTraverse.Add(new Point(x, y));
                }
            }

            var random = new Random(0);
            random.Shuffle(_districtTraverse);

            _districtQueue = new List<Tuple<double, District>>();
        }
        
        public void Load(XElement element)
        {
            _controlFlagTrait = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "required_world_trait",
                _gamedataTracker.Traits,
                t => t.Id);

            foreach(var e in element.Elements("adjacent_mod"))
            {
                var biome = _xmlReaderUtil.ObjectReferenceLookup(
                    e,
                    "biome_id",
                    _gamedataTracker.Biomes,
                    b => b.Id);

                var amount = _xmlReaderUtil.AttributeAsDouble(e, "amount");

                _adjacentMods[biome] = amount;
            }

            _worldHeight = _xmlReaderUtil.AttributeAsInt(element, "world_height");

            var biomeMappings = element
                .Elements("biome_mapping")
                .Select(e => new SeasonMapping<Biome>
                {
                    From = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "from",
                        _gamedataTracker.Biomes,
                        b => b.Id),
                    To = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "to",
                        _gamedataTracker.Biomes,
                        b => b.Id),
                    Threshold = _xmlReaderUtil.AttributeAsDouble(e, "threshold")
                })
                .ToArray();

            foreach (var mapping in biomeMappings)
            {
                if(!_biomeMappingsByFrom.ContainsKey(mapping.From))
                {
                    _biomeMappingsByFrom[mapping.From] = mapping;
                }
                if (!_biomeMappingsByTo.ContainsKey(mapping.To))
                {
                    _biomeMappingsByTo[mapping.To] = mapping;
                }
            }

            var cellFeatureMappings = element
                .Elements("cell_feature_mapping")
                .Select(e => new SeasonMapping<ICellFeature>
                {
                    From = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "from",
                        _gamedataTracker.CellFeatures,
                        cf => cf.Id),
                    To = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "to",
                        _gamedataTracker.CellFeatures,
                        cf => cf.Id),
                    Threshold = _xmlReaderUtil.AttributeAsDouble(e, "threshold")
                })
                .ToArray();

            foreach (var mapping in cellFeatureMappings)
            {
                if (!_cellFeatureMappingsByFrom.ContainsKey(mapping.From))
                {
                    _cellFeatureMappingsByFrom[mapping.From] = mapping;
                }
                if (!_cellFeatureMappingsByTo.ContainsKey(mapping.To))
                {
                    _cellFeatureMappingsByTo[mapping.To] = mapping;
                }
            }

            var terrainMappings = element
                .Elements("terrain_mapping")
                .Select(e => new SeasonMapping<Terrain>
                {
                    From = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "from",
                        _gamedataTracker.Terrain,
                        cf => cf.Id),
                    To = _xmlReaderUtil.ObjectReferenceLookup(
                        e,
                        "to",
                        _gamedataTracker.Terrain,
                        cf => cf.Id),
                    Threshold = _xmlReaderUtil.AttributeAsDouble(e, "threshold")
                })
                .ToArray();

            foreach (var mapping in terrainMappings)
            {
                if (!_terrainMappingsByFrom.ContainsKey(mapping.From))
                {
                    _terrainMappingsByFrom[mapping.From] = mapping;
                }
                if (!_terrainMappingsByTo.ContainsKey(mapping.To))
                {
                    _terrainMappingsByTo[mapping.To] = mapping;
                }
            }
        }

        public void PostLoad(XElement element)
        {
        }

        public void StepSimulation(double elapsedMonths)
        {
            if(!_worldManager.WorldTraits.Contains(_controlFlagTrait))
            {
                return;
            }

            var thresholdByDate = Math.Abs(((_worldManager.Month - 1) % 12) - 6) / 6.0 + 0.2;
            var halfWorldHeight = _worldHeight / 2;

            if (_nextIndex >= 0)
            {
                foreach (var cell in _cellsInOrder[_nextIndex])
                {
                    var thresholdByLat = Math.Abs(cell.Position.Y - halfWorldHeight) / (double)halfWorldHeight;
                    
                    var thresholdMod = cell.Region.Neighbors.Where(n=> _adjacentMods.ContainsKey(n.Biome)).Sum(n => _adjacentMods[n.Biome]);

                    if(cell.Region.Neighbors.Any())
                    {
                        thresholdMod = thresholdMod / cell.Region.Neighbors.Count;
                    }

                    var threshold = thresholdByDate * (thresholdByLat + _noiseMap[cell.Position.X, cell.Position.Y] / 2) + thresholdMod;
                    
                    if (_biomeMappingsByFrom.ContainsKey(cell.Biome) && threshold >= _biomeMappingsByFrom[cell.Biome].Threshold)
                    {
                        cell.Biome = _biomeMappingsByFrom[cell.Biome].To;

                    }
                    else if (_biomeMappingsByTo.ContainsKey(cell.Biome) && threshold < _biomeMappingsByTo[cell.Biome].Threshold)
                    {
                        cell.Biome = _biomeMappingsByTo[cell.Biome].From;
                    }

                    if (cell.Feature != null)
                    {
                        if (_cellFeatureMappingsByFrom.ContainsKey(cell.Feature) && threshold >= _cellFeatureMappingsByFrom[cell.Feature].Threshold)
                        {
                            cell.Feature = _cellFeatureMappingsByFrom[cell.Feature].To;

                        }
                        else if (_cellFeatureMappingsByTo.ContainsKey(cell.Feature) && threshold < _cellFeatureMappingsByTo[cell.Feature].Threshold)
                        {
                            cell.Feature = _cellFeatureMappingsByTo[cell.Feature].From;
                        }
                    }

                    if(cell.Region.Settlement != null)
                    {
                        var district = cell.Region.Settlement.Districts
                            .Where(d => d.IsGenerated && d.Cell == cell)
                            .FirstOrDefault();

                        if (district != null && !_districtQueue.Any(d => d.Item2 == district))
                        {
                            _districtQueue.Add(Tuple.Create(threshold, district));
                        }
                    }
                }

                _nextIndex -= 1;
            }
            else
            {
                _nextIndex = _cellsInOrder.Length - 1;
            }

            if(_districtQueue.Any())
            {
                var district = _districtQueue.First().Item2;
                var threshold = _districtQueue.First().Item1;

                for (var i = 0; i < 300 && _placeInDistrict < _districtTraverse.Count; i++)
                {
                    var tile = district.Tiles[_districtTraverse[_placeInDistrict].X, _districtTraverse[_placeInDistrict].Y];

                    if (_terrainMappingsByFrom.ContainsKey(tile.Terrain) && threshold >= _terrainMappingsByFrom[tile.Terrain].Threshold)
                    {
                        tile.Terrain = _terrainMappingsByFrom[tile.Terrain].To;

                    }
                    else if (_terrainMappingsByTo.ContainsKey(tile.Terrain) && threshold < _terrainMappingsByTo[tile.Terrain].Threshold)
                    {
                        tile.Terrain = _terrainMappingsByTo[tile.Terrain].From;
                    }

                    _placeInDistrict += 1;
                }

                if(_placeInDistrict >= _districtTraverse.Count)
                {
                    _placeInDistrict = 0;
                    _districtQueue.RemoveAt(0);
                }
            }
        }

        public void Clear()
        {
            _cellsInOrder = new[] { new Cell[0] };
            _nextIndex = 0;
            _districtQueue.Clear();
            _placeInDistrict = 0;
        }

        public void Initialize(WorldState worldState)
        {
            _cellsInOrder = worldState.Planes
                .SelectMany(p => p.Regions)
                .SelectMany(r => r.Cells)
                .GroupBy(c => c.Position.Y)
                .OrderBy(g => g.Key)
                .Select(g => g.ToArray())
                .ToArray();

            _noiseMap = new double[worldState.Size, worldState.Size];
            
            var noiseGenerator = new SimplexNoiseGenerator(worldState.Seed);
            for(var x = 0; x < worldState.Size; x++)
            {
                for (var y = 0; y < worldState.Size; y++)
                {
                    _noiseMap[x, y] = noiseGenerator.CoherentNoise(x, y, 0, 2, 25, 0.5f, 2, 0.9f);
                }
            }
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
        }

        private class SeasonMapping<T>
        {
            public T From { get; set; }
            public T To { get; set; }
            public double Threshold { get; set; }
        }
    }
}

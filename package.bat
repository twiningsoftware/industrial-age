@echo off 

REM ************************************************************
REM 					CONFIGURATION
REM ************************************************************

REM Change this to match the name of your mod
SET modname=IndustrialAge

REM Change this to your local install directory of 7-Zip
SET zippath="C:\Program Files\7-Zip\7z.exe"

REM ************************************************************
REM 					CLEANUP
REM ************************************************************

ECHO ***** Cleanup *****

DEL *.zip

RMDIR /Q /S working

REM ************************************************************
REM 					PACKAGING
REM ************************************************************

ECHO ***** Packaging *****

MKDIR working

ECHO Scripts
ROBOCOPY . working *.cs /S /NJH /NJS /NDL
RMDIR /Q /S working\obj

ECHO Data
ROBOCOPY . working *.xml /S /NJH /NJS /NDL

ECHO Namelists
ROBOCOPY . working *.csv /S /NJH /NJS /NDL

ECHO Images
ROBOCOPY . working *.png /S /NJH /NJS /NDL

RMDIR /Q /S working\working

cd working

%zippath% a ..\%modname%.zip "*"

cd ..

RMDIR /Q /S working

ECHO ***** Finished! *****
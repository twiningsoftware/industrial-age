﻿using Bronze.Common.Data.World.CellDevelopments;
using Bronze.Common.UI.Modals;
using Bronze.Contracts;
using Bronze.Contracts.Crisis;
using Bronze.Contracts.Data;
using Bronze.Contracts.Data.Dungeons;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.Serialization;
using Bronze.Contracts.Data.Simulation.Events;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using Bronze.UI.Modals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace IndustrialAge.Crises
{
    public class CommunismCrisis : ISubSimulator, IWorldStateService, IDataLoader, IMidgameCrisis, ISubversiveInfiltrationCrisis
    {
        public string ElementName => "communism_crisis";

        public string InfiltrationTypeId => typeof(CommunismCrisis).Name;

        public string Adjective => _cult.Adjective;

        private readonly IWorldManager _worldManager;
        private readonly IGamedataTracker _gamedataTracker;
        private readonly IPlayerDataTracker _playerData;
        private readonly INotificationsSystem _notificationsSystem;
        private readonly IGenericNotificationBuilder _genericNotificationBuilder;
        private readonly IUnitHelper _unitHelper;
        private readonly IXmlReaderUtil _xmlReaderUtil;
        private readonly IWarHelper _warHelper;
        private readonly IBattleHelper _battleHelper;
        private readonly IPeopleGenerator _peopleGenerator;
        private readonly ISimulationEventBus _simulationEventBus;
        private readonly IPeopleHelper _peopleHelper;
        private readonly INameSource _nameSource;
        private readonly IDungeonDataManager _dungeonDataManager;
        private readonly IDialogManager _dialogManager;
        private readonly IRebellionManager _rebellionManager;
        private readonly Random _random;

        private Trait _worldEnableTrait;
        private Cult _cult;
        private BonusType _authorityBonus;
        private BonusType _strifeBonus;
        private NotablePersonSkill _injurySkill;
        private ITribeControllerType _banditControllerType;
        private Trait _cultTribeTrait;
        private ITribeControllerType _rebelControllerType;
        private double _growRate;
        private double _eventRate;
        private double _infiltrationInfluence;
        private double _investigationDifficulty;
        private DungeonDefinition _hideoutDungeon;

        private bool _playerKnowsAboutCult;
        private Dictionary<Settlement, double> _lastEventCheck;
        private Dictionary<Settlement, double> _progress;


        public CommunismCrisis(
            IWorldManager worldManager,
            IGamedataTracker gamedataTracker,
            IPlayerDataTracker playerData,
            INotificationsSystem notificationsSystem,
            IGenericNotificationBuilder genericNotificationBuilder,
            IUnitHelper unitHelper,
            IXmlReaderUtil xmlReaderUtil,
            IWarHelper warHelper,
            IBattleHelper battleHelper,
            IPeopleGenerator peopleGenerator,
            ISimulationEventBus simulationEventBus,
            IPeopleHelper peopleHelper,
            INameSource nameSource,
            IDungeonDataManager dungeonDataManager,
            IDialogManager dialogManager,
            IRebellionManager rebellionManager)
        {
            _worldManager = worldManager;
            _gamedataTracker = gamedataTracker;
            _playerData = playerData;
            _notificationsSystem = notificationsSystem;
            _genericNotificationBuilder = genericNotificationBuilder;
            _unitHelper = unitHelper;
            _warHelper = warHelper;
            _xmlReaderUtil = xmlReaderUtil;
            _battleHelper = battleHelper;
            _peopleGenerator = peopleGenerator;
            _simulationEventBus = simulationEventBus;
            _peopleHelper = peopleHelper;
            _nameSource = nameSource;
            _dungeonDataManager = dungeonDataManager;
            _dialogManager = dialogManager;
            _rebellionManager = rebellionManager;
            _random = new Random();

            _lastEventCheck = new Dictionary<Settlement, double>();
            _progress = new Dictionary<Settlement, double>();
        }

        public void Clear()
        {
            _playerKnowsAboutCult = false;
            _lastEventCheck.Clear();
            _progress = new Dictionary<Settlement, double>();
        }

        public void Initialize(WorldState worldState)
        {
            _playerKnowsAboutCult = false;
        }

        public void Load(XElement element)
        {
            _worldEnableTrait = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "world_enable_trait",
                _gamedataTracker.Traits,
                t => t.Id);

            _growRate = _xmlReaderUtil.AttributeAsDouble(element, "grow_rate");

            _eventRate = _xmlReaderUtil.AttributeAsDouble(element, "event_rate");

            _infiltrationInfluence = _xmlReaderUtil.AttributeAsDouble(element, "infiltration_influence");

            _investigationDifficulty = _xmlReaderUtil.AttributeAsDouble(element, "investigation_difficulty");

            _cult = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "cult",
                _gamedataTracker.Cults,
                c => c.Id);

            _authorityBonus = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "authority_bonus",
                _gamedataTracker.BonusTypes,
                b => b.Id);

            _strifeBonus = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "strife_bonus",
                _gamedataTracker.BonusTypes,
                b => b.Id);

            _injurySkill = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "injury_skill",
                _gamedataTracker.Skills,
                b => b.Id);

            _banditControllerType = _xmlReaderUtil.ObjectReferenceLookup(
                    element,
                    "bandit_controller_type",
                    _gamedataTracker.ControllerTypes,
                    b => b.Id);

            _cultTribeTrait = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "communism_tribe_trait",
                _gamedataTracker.Traits,
                t => t.Id);

            _rebelControllerType = _xmlReaderUtil.ObjectReferenceLookup(
                    element,
                    "rebel_controller_type",
                    _gamedataTracker.ControllerTypes,
                    b => b.Id);

            _hideoutDungeon = _xmlReaderUtil.ObjectReferenceLookup(
                element,
                "hideout_dungeon",
                _dungeonDataManager.DungeonDefinitions,
                d => d.Id);
        }

        public void LoadCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.GetOptionalChild("communism_crisis");

            if (serviceRoot != null)
            {
                _playerKnowsAboutCult = serviceRoot.GetOptionalBool("player_knows");

                var allSettlements = _worldManager.Tribes.SelectMany(t => t.Settlements).ToArray();

                foreach (var kvpRoot in serviceRoot.GetChildren("state"))
                {
                    var settlement = kvpRoot.GetOptionalObjectReferenceOrDefault(
                        "settlement",
                        allSettlements,
                        s => s.Id);
                    var lastCheck = kvpRoot.GetDouble("last_check");
                    var progress = kvpRoot.GetDouble("progress");

                    if (settlement != null)
                    {
                        _lastEventCheck.Add(settlement, lastCheck);
                        _progress.Add(settlement, progress);
                    }
                }
            }
        }

        public void PostLoad(XElement element)
        {
        }

        public void SaveCustomData(SerializedObject worldRoot)
        {
            var serviceRoot = worldRoot.CreateChild("communism_crisis");

            serviceRoot.Set("player_knows", _playerKnowsAboutCult);

            foreach (var settlement in _progress.Keys)
            {
                var kvpRoot = serviceRoot.CreateChild("state");
                kvpRoot.Set("settlement", settlement.Id);
                kvpRoot.Set("last_check", _lastEventCheck[settlement]);
                kvpRoot.Set("progress", _progress[settlement]);
            }
        }

        public void StepSimulation(double elapsedMonths)
        {
            foreach (var settlement in _progress.Keys.ToArray())
            {
                if (settlement.IsRemoved)
                {
                    _progress.Remove(settlement);
                    _lastEventCheck.Remove(settlement);
                }
                else
                {
                    if (_progress[settlement] < 1)
                    {
                        _progress[settlement] += _growRate * elapsedMonths;
                    }

                    if (_progress[settlement] < 2
                        && _worldManager.Month > _lastEventCheck[settlement] + 1)
                    {
                        _lastEventCheck[settlement] = _worldManager.Month;

                        UpdateInfiltrationDebuff(settlement);

                        if (_random.NextDouble() < _eventRate * _progress[settlement])
                        {
                            DoEventFor(settlement);
                        }
                    }
                }
            }
        }

        private void DoEventFor(Settlement settlement)
        {
            if (settlement.CultMembership[_cult] == null)
            {
                return;
            }

            if (settlement.CultMembership[_cult].Count < 1
                && _progress[settlement] >= 1)
            {
                _progress.Remove(settlement);
                _lastEventCheck.Remove(settlement);
                return;
            }

            var roll = _random.NextDouble();

            var settlementPos = settlement.EconomicActors
                    .OfType<ICellDevelopment>()
                    .Where(cd => cd is DistrictDevelopment || cd is VillageDevelopment)
                    .Select(cd => cd.Cell.Position)
                    .FirstOrDefault();

            var membershipPercent = settlement.CultMembership[_cult]?.Count / (double)settlement.Population.Count;

            if (membershipPercent > 0.05)
            {
                if (membershipPercent < 0.1)
                {
                    DoUnrestDebuff(settlement, roll);
                }
                else if (membershipPercent < 0.25)
                {
                    if (roll > 0.75 && (settlement == settlement.Owner.Capital
                        || (settlement?.Province?.Capital == settlement && settlement?.Province?.Governor?.Holder != null)))
                    {
                        DoCharacterAssault(settlement, settlementPos);
                    }
                    else if (roll > 0.5)
                    {
                        DoPopAssault(settlement, settlementPos);
                    }
                    else if (roll > 0.25)
                    {
                        DoUnrestDebuff(settlement, roll);
                    }
                    else
                    {
                        DoBandits(settlement, settlementPos);
                    }
                }
                else if (membershipPercent < 0.5)
                {
                    if (roll > 0.85 && (settlement == settlement.Owner.Capital
                        || (settlement?.Province?.Capital == settlement && settlement?.Province?.Governor?.Holder != null)))
                    {
                        DoCharacterAssault(settlement, settlementPos);
                    }
                    else if (roll > 0.7)
                    {
                        DoPopAssault(settlement, settlementPos);
                    }
                    else if (roll > 0.6)
                    {
                        DoUnrestDebuff(settlement, roll);
                    }
                    else
                    {
                        DoBandits(settlement, settlementPos);
                    }
                }
                else
                {
                    DoRevolt(settlement, settlementPos);
                }
            }
        }

        private void UpdateInfiltrationDebuff(Settlement settlement)
        {
            var buff = settlement.Buffs
                    .Where(b => b.Id == "buff_communism_infiltration")
                    .FirstOrDefault();

            if (buff == null)
            {
                buff = new Buff
                {
                    Id = "buff_communism_infiltration",
                    IconKey = "buff_communism_infiltration",
                    Name = "Communist Infiltration",
                    Description = string.Empty,
                    ExpireMonth = -1
                };
                settlement.AddBuff(buff);
            }

            buff.AppliedInfluence[_cult] = _progress[settlement] * _infiltrationInfluence;
        }

        private void DoUnrestDebuff(Settlement settlement, double roll)
        {
            Buff debuff;
            if (roll < 0.5)
            {
                debuff = new Buff
                {
                    Id = "buff_communist_unrest_0",
                    IconKey = "buff_communism_infiltration",
                    Name = "Subversive Activity",
                    Description = "Subversive communist activity is eroding authority.",
                    ExpireMonth = _lastEventCheck[settlement]
                };
                debuff.AppliedBonuses[_authorityBonus] = -0.5;
                debuff.AppliedInfluence[_cult] = 40;
            }
            else
            {
                debuff = new Buff
                {
                    Id = "buff_communist_unrest_1",
                    IconKey = "buff_communism_infiltration",
                    Name = "Social Unrest",
                    Description = "Subversive communist activity is causing unrest.",
                    ExpireMonth = _lastEventCheck[settlement]
                };
                debuff.AppliedBonuses[_strifeBonus] = -0.5;
                debuff.AppliedInfluence[_cult] = 40;
            }

            settlement.AddBuff(debuff);

            if (settlement.Owner == _playerData.PlayerTribe)
            {
                _notificationsSystem.AddNotification(_genericNotificationBuilder
                        .BuildBuffNotification(
                            "ui_notification_communist_unrest",
                            $"Unrest in {settlement.Name}",
                            $"{_cult.Adjective} revolutionaries are causing problems.",
                            $"Unrest in {settlement.Name}",
                            $"The {_cult.Name} presence in {settlement.Name} is causing problems.",
                            settlement,
                            debuff));
            }
        }

        private void DoPopAssault(Settlement settlement, CellPosition settlementPos)
        {
            var potentialTargets = settlement.Population
                .Where(p => p.CultMembership != _cult)
                .ToList();

            if (potentialTargets.Any())
            {
                var count = Math.Max(1, _random.Next(2, 5) / 100.0 * settlement.Population.Count);
                var victims = new List<Pop>();

                for (var i = 0; i < count && settlement.Population.Count > 1 && potentialTargets.Any(); i++)
                {
                    var victim = _random.Choose(potentialTargets);
                    settlement.Population.Remove(victim);
                    potentialTargets.Remove(victim);
                    victims.Add(victim);
                }

                settlement.SetCalculationFlag("pop murders");

                if (_playerData.PlayerTribe == settlement.Owner)
                {
                    var body = $"Street violence in {settlement.Name} beween {_cult.Adjective} sympathisers and loyal citzens has left several people dead.";
                    body += $"\nVictims:";

                    foreach (var group in victims.GroupBy(p => p.Caste))
                    {
                        if (group.Count() > 1)
                        {
                            body += $"\n{group.Count()} {group.Key.NamePlural}";
                        }
                        else
                        {
                            body += $"\n1 {group.Key.Name}";
                        }
                    }

                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_communist_violence",
                            $"Street Violence in {settlement.Name}",
                            $"There's been an eruption of street violence in {settlement.Name}.",
                            $"Street Violence in {settlement.Name}",
                            body,
                            settlementPos));
                }
            }
        }

        private void DoCharacterAssault(Settlement settlement, CellPosition settlementPos)
        {
            var potentialTargets = new List<NotablePerson>();

            var governor = settlement?.Province?.Governor?.Holder;

            if (settlement?.Province?.Capital == settlement && governor != null)
            {
                potentialTargets.Add(governor);
                if (governor.Spouse != null && governor.Spouse.IsIdle)
                {
                    potentialTargets.Add(governor.Spouse);
                }
                potentialTargets.AddRange(governor.Children.Where(c => c.IsIdle || !c.IsMature));
            }

            if (settlement == settlement.Owner.Capital)
            {
                potentialTargets.Add(settlement.Owner.Ruler);
                if (settlement.Owner.Ruler.Spouse != null && settlement.Owner.Ruler.Spouse.IsIdle)
                {
                    potentialTargets.Add(settlement.Owner.Ruler.Spouse);
                }
                potentialTargets.AddRange(settlement.Owner.Ruler.Children.Where(c => c.IsIdle || !c.IsMature));

                potentialTargets.AddRange(settlement.Owner.NotablePeople.Where(c => c.IsIdle || !c.IsMature));
            }

            potentialTargets.RemoveAll(np => np == null || np.IsDead);

            if (potentialTargets.Any())
            {
                var target = _random.Choose(potentialTargets);

                var skillPlacement = _peopleHelper.IncrementOrAdd(target, _injurySkill);

                if ((skillPlacement?.CanIncrement() ?? false) && _random.NextDouble() < 0.5)
                {
                    skillPlacement = _peopleHelper.IncrementOrAdd(target, _injurySkill);

                    if ((skillPlacement?.CanIncrement() ?? false) && _random.NextDouble() < 0.5)
                    {
                        skillPlacement = _peopleHelper.IncrementOrAdd(target, _injurySkill);
                    }
                }

                if (skillPlacement != null)
                {
                    var doDeath = _random.NextDouble() <= target.GetMinorStat("Death Chance");

                    if (doDeath)
                    {
                        _peopleHelper.Kill(target);
                    }
                    else if (_playerData.PlayerTribe == settlement.Owner)
                    {
                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                            .BuildPersonSkillNotification(
                                "ui_notification_communist_assassin",
                                $"{target.Name} Injured",
                                $"{target.Name} has been injured by an assassin in {settlement.Name}.",
                                $"{target.Name} Injured",
                                $"A {_cult.Adjective} assassin in {settlement.Name} has injured {target.Name} in a suprise attack.",
                                target,
                                skillPlacement,
                                settlementPos));
                    }
                }
            }
        }

        private void DoBandits(Settlement settlement, CellPosition settlementPos)
        {
            var recruits = new List<Tuple<IUnitType, UnitEquipmentSet>>();
            foreach (var kvp in _cult.RecruitmentSlots)
            {
                var options = settlement.Race.AvailableUnits
                    .Where(ut => ut.RecruitmentCategory == kvp.Key)
                    .SelectMany(ut => ut.EquipmentSets
                        .Where(es => es.RecruitmentCost.All(iq => settlement.ItemAvailability[iq.Item]))
                        .Select(es => Tuple.Create(ut, es)))
                    .ToArray();

                if (options.Any())
                {
                    recruits.AddRange(_random.ChooseCount(options, kvp.Value));
                }
            }

            if (!recruits.Any())
            {
                return;
            }

            var potentials = settlement.Region.Cells
                .Where(c => recruits.Any(t => t.Item2.CanPath(c)))
                .ToArray();

            if (potentials.Any())
            {
                var banditTribe = _worldManager.Tribes
                    .Where(t => t.Name == _cult.Adjective + " Bandits")
                    .FirstOrDefault();

                if (banditTribe == null)
                {
                    banditTribe = new Tribe(settlement.Race)
                    {
                        PrimaryColor = BronzeColor.Black,
                        SecondaryColor = BronzeColor.Grey,
                        Name = _cult.Adjective + " Bandits",
                        IsBandits = true
                    };
                    _worldManager.Tribes.Add(banditTribe);

                    banditTribe.Controller = _banditControllerType.BuildController(
                        banditTribe,
                        _random,
                        _worldManager.Hostility,
                        _worldManager.Month);
                }

                var unitsPlaced = false;

                var army = _unitHelper.CreateArmy(banditTribe, _random.Choose(potentials));

                foreach (var unitToSpawn in recruits)
                {
                    if (army.IsFull)
                    {
                        army = _unitHelper.CreateArmy(banditTribe, _random.Choose(potentials));
                    }

                    if (unitToSpawn.Item2.CanPath(army.Cell))
                    {
                        army.Units.Add(unitToSpawn.Item1.CreateUnit(unitToSpawn.Item2));
                        unitsPlaced = true;
                    }
                }

                if (unitsPlaced && _playerData.PlayerTribe == settlement.Owner)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildSimpleNotification(
                            "ui_notification_communist_bandits",
                            $"{_cult.Adjective} bandits in {settlement.Name}",
                            $"{_cult.Adjective} bandits have been spotted near {settlement.Name}.",
                            $"{_cult.Adjective} bandits in {settlement.Name}",
                            $"The {_cult.Name} has grown in strength in {settlement.Name} to the point where cultists are taking to the countryside, raiding and pillaging in the name of their twisted god.",
                            settlementPos));
                }
            }
        }

        private void DoRevolt(Settlement settlement, CellPosition settlementPos)
        {
            var oldOwner = settlement.Owner;

            var neighborsToJoin = settlement.Region.Neighbors
                .Where(n => n.Settlement != null && n.Settlement.Owner.Traits.Contains(_cultTribeTrait))
                .Select(n => n.Owner)
                .Distinct()
                .ToArray();

            var joinedExisting = neighborsToJoin.Any();
            Tribe newOwner;
            int traitorArmies;

            if (!neighborsToJoin.Any())
            {
                var revoltResult = _rebellionManager.DoRevolt(
                    settlement,
                    settlementPos,
                    _rebelControllerType,
                    settlement.Race);

                newOwner = settlement.Owner;

                settlement.Owner.Traits.Add(_cultTribeTrait);

                foreach (var person in settlement.Owner.NotablePeople)
                {
                    person.CultMembership = _cult;
                }

                traitorArmies = revoltResult.TraitorArmies.Count;
            }
            else
            {
                newOwner = _random.Choose(neighborsToJoin);

                if (!_warHelper.AreAtWar(oldOwner, newOwner))
                {
                    _warHelper.StartWar(oldOwner, newOwner);
                }

                var armiesToClaim = settlement.Owner.Armies
                    .Where(a => a.Cell.Region == settlement.Region && a.General == null)
                    .ToList();

                traitorArmies = armiesToClaim.Count;

                foreach (var army in armiesToClaim)
                {
                    army.ChangeOwner(newOwner);
                    army.CurrentOrder = null;
                }

                _simulationEventBus.SendEvent(new SettlementOwnerChangedSimulationEvent
                {
                    OldOwner = oldOwner,
                    NewOwner = newOwner,
                    Settlement = settlement
                });

                settlement.ChangeOwner(newOwner);
            }

            if (_playerData.PlayerTribe == oldOwner)
            {
                var body = $"{_cult.Adjective} influence in {settlement.Name} has grown powerful enough for them to seize power for themselves. Your representatives have been thrown out of the city";
                if (traitorArmies > 0)
                {
                    body += $", and {traitorArmies} of your armies have joined the traitors.";
                }
                else
                {
                    body += ".";
                }

                _notificationsSystem.AddNotification(
                    _genericNotificationBuilder.BuildSimpleNotification(
                        "ui_notification_communist_revolt",
                        $"Rebellion in {settlement.Name}",
                        $"{_cult.Adjective} partisans in {settlement.Name} have sized power.",
                        $"Rebellion in {settlement.Name}",
                        body,
                        settlementPos,
                        autoOpen: true));
            }
            else if (_playerData.PlayerTribe.HasKnowledgeOf(oldOwner))
            {
                _notificationsSystem.AddNotification(
                    _genericNotificationBuilder.BuildSimpleNotification(
                        "ui_notification_communist_revolt",
                        $"News of Rebellion",
                        $"{_cult.Adjective} partisans have sized control in {settlement.Name}.",
                        $"News of Rebellion",
                        $"{_cult.Adjective} partisans in {settlement.Name} have seizes power from their rightful rulers.",
                        settlementPos,
                        autoOpen: true));
            }

            _progress[settlement] = 2;
            ClearInfiltrationDebuff(settlement);
        }

        public bool IsPossibleForWorld()
        {
            return _worldManager.WorldTraits.Contains(_worldEnableTrait);
        }

        public void StartNew()
        {
            var options = _worldManager.Tribes
                .SelectMany(t => t.Settlements)
                .Where(s => !_progress.ContainsKey(s))
                .Where(s => s.Region.Neighbors.Select(r => r.Settlement).Where(n => n != null).All(n => !_progress.ContainsKey(n)))
                .Where(s => !s.Owner.Traits.Contains(_cultTribeTrait))
                .ToArray();

            var likelyhoods = options
                .Select(s => new LikelyhoodFor<Settlement>
                {
                    Value = s,
                    Likelyhood = 1 + s.Region.Neighbors.Where(n => n.Settlement != null).Count()
                });

            if (likelyhoods.Any())
            {
                var target = _random.ChooseByLikelyhood(likelyhoods);

                _progress.Add(target, 0);
                _lastEventCheck.Add(target, _worldManager.Month);

                if (_playerData.PlayerTribe == target.Owner)
                {
                    if (_playerKnowsAboutCult)
                    {
                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                            .BuildSimpleNotification(
                                "ui_notification_communist_start",
                                $"{_cult.Adjective} Agitators Sighted in {target.Name}!",
                                $"{_cult.Adjective} agitators has been sighted again.",
                                $"{_cult.Adjective} Agitators Sighted in {target.Name}",
                                $"{_cult.Adjective} agitators have been sighted again, this time in {target.Name}.",
                                target.EconomicActors.OfType<ICellDevelopment>()
                                    .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                                    .Select(cd => cd.Cell.Position)
                                    .FirstOrDefault()));
                    }
                    else
                    {
                        _playerKnowsAboutCult = true;

                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                            .BuildSimpleNotification(
                                "ui_notification_communist_start",
                                $"Political Revolutionaries in {target.Name}!",
                                $"A new group of political revolutionaries have been sighted in one of your settlements.",
                                $"Political Revolutionaries in {target.Name}",
                                $"A new group of political revolutionaries have been sighted in {target.Name}. They call for the rise of the working classes, proclaiming {_cult.Name}.",
                                target.EconomicActors.OfType<ICellDevelopment>()
                                    .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                                    .Select(cd => cd.Cell.Position)
                                    .FirstOrDefault()));
                    }
                }
                else if (_playerData.PlayerTribe.HasKnowledgeOf(target.Owner))
                {
                    if (_playerKnowsAboutCult)
                    {
                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                            .BuildSimpleNotification(
                                "ui_notification_communist_start",
                                $"{_cult.Adjective} Agitators Sighted in {target.Name}!",
                                $"{_cult.Adjective} agitators has been sighted again.",
                                $"{_cult.Adjective} Agitators Sighted in {target.Name}!",
                                $"The {_cult.Name} have been sighted again, this time in {target.Name}.",
                                target.EconomicActors.OfType<ICellDevelopment>()
                                    .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                                    .Select(cd => cd.Cell.Position)
                                    .FirstOrDefault()));
                    }
                    else
                    {
                        _playerKnowsAboutCult = true;

                        _notificationsSystem.AddNotification(_genericNotificationBuilder
                            .BuildSimpleNotification(
                                "ui_notification_communist_start",
                                $"Political Revolutionaries in {target.Name}!",
                                $"A new group of political revolutionaries have been sighted in the lands of the {target.Owner.Name}.",
                                $"Political Revolutionaries in {target.Name}",
                                $"A new group of political revolutionaries have been sighted in the lands of the {target.Owner.Name}. They call for the rise of the working classes, proclaiming {_cult.Name}.",
                                target.EconomicActors.OfType<ICellDevelopment>()
                                    .Where(cd => cd is VillageDevelopment || cd is DistrictDevelopment)
                                    .Select(cd => cd.Cell.Position)
                                    .FirstOrDefault()));
                    }
                }
            }
        }

        public bool Affects(Settlement settlement)
        {
            return _progress.ContainsKey(settlement)
                && _progress[settlement] < 2;
        }

        public double InvestigationChance(
            Settlement settlement,
            NotablePerson investigator,
            int investigtions)
        {
            if (_progress.ContainsKey(settlement))
            {
                var adjustedDifficulty = Util.Clamp(1 - _progress[settlement], 0.1, 1) * _investigationDifficulty;

                return (investigator.GetStat(NotablePersonStatType.Wit) - adjustedDifficulty) * 0.05 + 0.5;
            }

            return 0;
        }

        public double DiscoveryChance(Settlement settlement, NotablePerson investigator, int investigtions)
        {
            var adjustedDifficulty = (3 - investigtions) * _investigationDifficulty;

            return (investigator.GetStat(NotablePersonStatType.Wit) - adjustedDifficulty) * 0.05 + 0.5;
        }

        public InvestigationEventResult InvestigationEvent(
            Settlement settlement,
            NotablePerson investigator,
            int investigations)
        {
            if (!_progress.ContainsKey(settlement))
            {
                return new InvestigationEventResult();
            }

            var random = new Random();

            var r = _progress[settlement] + random.NextDouble() + investigations / 10.0;

            if (r <= 0.5)
            {
                return DoRumorsInvestigationEvent(settlement, investigator, investigations);
            }
            else if (r <= 1.0)
            {
                return DoCatchInvestigationsEvent(settlement, investigator);
            }
            else if (r <= 1.5)
            {
                return DoAssassinationAttemptInvestigationsEvent(settlement, investigator);
            }

            return new InvestigationEventResult();
        }

        private InvestigationEventResult DoRumorsInvestigationEvent(Settlement settlement, NotablePerson investigator, int investigations)
        {
            if (investigator.Owner == _playerData.PlayerTribe)
            {

                string body;

                if (investigations > 1)
                {
                    body = $"{investigator.Name}'s has found further rumors of {Adjective} influence in {settlement.Name}, leading {investigator.HimHer} closer to the traitors responsible.";
                }
                else
                {
                    body = $"{investigator.Name}'s investigation into traitors in {settlement.Name} has revealed evidence of {Adjective} influence in the settlement. Further ";
                }

                _notificationsSystem.AddNotification(
                    _genericNotificationBuilder.BuildPersonNotification(
                        "task_find_traitors",
                        $"Investigation Stalled",
                        $"{investigator.Name}'s investigation in {settlement.Name} has stalled.",
                        $"Investigation Stalled",
                        body,
                        investigator,
                        settlement.EconomicActors.OfType<ISettlementCenterDevelopment>().Select(x => x.Cell.Position).FirstOrDefault()));
            }

            return new InvestigationEventResult();
        }

        private InvestigationEventResult DoCatchInvestigationsEvent(Settlement settlement, NotablePerson investigator)
        {
            var result = new InvestigationEventResult();

            var catchDifficulty = _investigationDifficulty;

            var didCatch = _random.NextDouble() < (investigator.GetStat(NotablePersonStatType.Valor) - catchDifficulty) * 0.05 + 0.5;

            if (didCatch)
            {
                result.DeltaInvestigations += 1;
            }

            if (investigator.Owner == _playerData.PlayerTribe)
            {
                if (didCatch)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildPersonNotification(
                            "task_find_traitors",
                            $"Traitor Captured",
                            $"{investigator.Name} caught a traitor.",
                            $"Traitor Captured",
                            $"In the course of {investigator.Name}'s investigations in {settlement.Name}, they succeeded in catching a traitor. While not deep into the conspiracy, vigorous questioning has yielded information that will dramatically accelerate the investigation of {Adjective} influence.",
                            investigator,
                            settlement.EconomicActors.OfType<ISettlementCenterDevelopment>().Select(x => x.Cell.Position).FirstOrDefault()));
                }
                else
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildPersonNotification(
                            "task_find_traitors",
                            $"Near Miss",
                            $"{investigator.Name} nearly caught a traitor.",
                            $"Near Miss",
                            $"In the course of {investigator.Name}'s investigations in {settlement.Name}, they nearly caught a traitor. While dissapointing, it shows that {investigator.HeShe} is close to finding the traitors responsible for {Adjective} influence in the settlement.",
                            investigator,
                            settlement.EconomicActors.OfType<ISettlementCenterDevelopment>().Select(x => x.Cell.Position).FirstOrDefault()));
                }
            }

            return result;
        }

        private InvestigationEventResult DoAssassinationAttemptInvestigationsEvent(Settlement settlement, NotablePerson investigator)
        {
            var result = new InvestigationEventResult();

            var catchDifficulty = _investigationDifficulty;

            var avoidedInjury = _random.NextDouble() < (investigator.GetStat(NotablePersonStatType.Valor) - catchDifficulty) * 0.05 + 0.5;

            NotablePersonSkillPlacement injuryPlacement = null;
            bool doDeath = false;

            if (!avoidedInjury)
            {
                injuryPlacement = _peopleHelper.IncrementOrAdd(investigator, _injurySkill);

                doDeath = _random.NextDouble() <= investigator.GetMinorStat("Death Chance");

                if (doDeath)
                {
                    _peopleHelper.Kill(investigator);
                }
            }

            if (investigator.Owner == _playerData.PlayerTribe)
            {
                if (injuryPlacement == null)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildPersonNotification(
                            "task_find_traitors",
                            $"{investigator.Name} Attacked",
                            $"{investigator.Name} was attacked!",
                            $"{investigator.Name} Attacked",
                            $"{investigator.Name} has fended off an assassination attempt. Their investigations into {Adjective} influence in {settlement.Name} seem to have the traitors scared.",
                            investigator,
                            settlement.EconomicActors.OfType<ISettlementCenterDevelopment>().Select(x => x.Cell.Position).FirstOrDefault()));
                }
                else if (!doDeath)
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildPersonSkillNotification(
                            "task_find_traitors",
                            $"{investigator.Name} Attacked",
                            $"{investigator.Name} was attacked and injured!",
                            $"{investigator.Name} Attacked",
                            $"{investigator.Name} has has been attacked by an assassin. While they survived the encounter, they were injured. This open rebellion cannot be allowed.",
                            investigator,
                            injuryPlacement,
                            settlement.EconomicActors.OfType<ISettlementCenterDevelopment>().Select(x => x.Cell.Position).FirstOrDefault()));
                }
                else
                {
                    _notificationsSystem.AddNotification(
                        _genericNotificationBuilder.BuildPersonNotification(
                            "task_find_traitors",
                            $"{investigator.Name} Assassinated!",
                            $"{investigator.Name} was assassinated!",
                            $"{investigator.Name} Assassinated!",
                            $"{Adjective} assassins attacked and killed {investigator.Name}, who was investigating {Adjective} influence in {settlement.Name}. Honor demands revenge!",
                            investigator,
                            settlement.EconomicActors.OfType<ISettlementCenterDevelopment>().Select(x => x.Cell.Position).FirstOrDefault()));
                }
            }

            return result;
        }

        public void DiscoveryEvent(
            Settlement settlement,
            NotablePerson investigator)
        {
            if (investigator.Owner == _playerData.PlayerTribe)
            {

                _dialogManager.PushModal<ConfirmModal, ConfirmModalContext>(
                    new ConfirmModalContext
                    {
                        Title = $"{Adjective} Hideout Found",
                        Body = $"There has been a breakthough in {investigator.Name}'s search for the source of {Adjective} influence in {settlement.Name}. {investigator.HeSheCap} believes to have found the secret hideout of the traitors.\n{investigator.Name} is ready to launch a raid, should {investigator.HeShe} continue?",
                        YesAction = () =>
                        {
                            GenerateAndRunDungeon(settlement, investigator);
                        },
                        NoAction = () =>
                        {
                        }
                    });
            }
            else
            {
                _progress[settlement] = 2;
                ClearInfiltrationDebuff(settlement);
            }
        }

        private void ClearInfiltrationDebuff(Settlement settlement)
        {
            var buff = settlement.Buffs
                    .Where(b => b.Id == "buff_communism_infiltration")
                    .FirstOrDefault();

            if (buff != null)
            {
                settlement.RemoveBuff(buff);
            }
        }

        private void GenerateAndRunDungeon(Settlement settlement, NotablePerson investigator)
        {
            var cityLocation = settlement.EconomicActors
                .OfType<ISettlementCenterDevelopment>()
                .Select(x => x.Cell)
                .FirstOrDefault();

            var bodyguardOptions = Util.GetStandardInfiltratorDungeonBodyguardOptions(settlement, cityLocation, _hideoutDungeon);

            var generatedDungeon = new GeneratedDungeon(_random.Next(), _hideoutDungeon);

            _dialogManager.PushModal<RunDungeonDialog, RunDungeonDialogContext>(
                    new RunDungeonDialogContext
                    {
                        GeneratedDungeon = generatedDungeon,
                        Character = investigator,
                        Army = null,
                        DungeonLocation = cityLocation,
                        BodyguardOptions = bodyguardOptions,
                        DoneCallback = () =>
                        {
                            if (generatedDungeon.State["cleared"] == "true")
                            {
                                _progress[settlement] = 2;
                                ClearInfiltrationDebuff(settlement);
                            }
                        }
                    });
        }
    }
}

﻿using Bronze.Contracts;
using IndustrialAge.Crises;
using IndustrialAge.Events;
using IndustrialAge.WorldGen;

namespace IndustrialAge
{
    public class IndustrialAgeInjectionModule : IInjectionModule
    {
        public void RegisterInjections(IInjector injector)
        {
            // Services
            injector.RegisterService<MutatedHexRegionGenerator>();
            injector.RegisterService<LastNameInheritorPersongenAddon>();
            injector.RegisterService<SeasonalBiomeSwapper>();

            // Crises
            injector.RegisterService<CommunismCrisis>();
        }
    }
}
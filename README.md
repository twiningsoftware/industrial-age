# Industrial Age

A total conversion mod for Bronze Age 3.0, replacing the ancient setting with diesel-punk. Industrial Age is designed to serve as an example to anyone interested in making mods for Bronze Age.

## Building Industrial Age


1. Edit `deploy.bat` to change `bronzepath` to point at your local Bronze Age location.
2. Make any changes you want.
3. Run `deploy.bat` to package up the mod, and place it in the mod directory for Bronze Age.

To distribute the mod to other people, (if you have 7-Zip installed) you can run `package.bat`. Otherwise, you can just zip up the folder created by `deploy.bat`.


## How It Works


### Game Data and Graphics Changes

All .xml, .png, and .csv files are copied over to the output directory. These are loaded into Bronze Age during the load process.

### Logic Changes

All .cs files are copied over to the output directory. Early in the load process these files are compiled and loaded into the Bronze Age execution environment. Logic changes are primarily applied through `IndustrialAgeInjectionModule.cs` new services, UnitTypes, StructureTypes, and CellDevelopmentTypes can be registered through that file, and made available to the rest of the game.

As an example, a new service `MutatedHexRegionGenerator` is registered. This is then referenced in `wg_ia_corus.xml` to provide a new way to generate regions in the world. 

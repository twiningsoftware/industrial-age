﻿using System;
using System.Linq;
using Bronze.Contracts.Data.Gamedata;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;

namespace IndustrialAge.WorldGen
{
    public class LastNameInheritorPersongenAddon : INotablePersonGenerationAddon
    {
        public void Apply(Random random, Tribe owner, Race race, bool inDynasty, NotablePerson person)
        {
            if (person.Race.Id == "u_r_union")
            {
                var lastName = string.Empty;

                if (person.Father != null && person.Father.Name.Contains(" "))
                {
                    lastName = person.Father.Name.Split(' ').Last();
                }
                else if (person.Mother != null && person.Mother.Name.Contains(" "))
                {
                    lastName = person.Mother.Name.Split(' ').Last();
                }

                if (!string.IsNullOrWhiteSpace(lastName))
                {
                    var tokenizedName = person.Name.Split(' ');

                    if (tokenizedName.Length > 1)
                    {
                        tokenizedName[tokenizedName.Length - 1] = lastName;
                    }

                    person.Name = string.Join(" ", tokenizedName);
                }
            }
        }
    }
}

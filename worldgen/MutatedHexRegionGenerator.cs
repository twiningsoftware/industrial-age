﻿using Bronze.Contracts;
using Bronze.Contracts.Data.World;
using Bronze.Contracts.InjectableServices;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IndustrialAge.WorldGen
{
    public class MutatedHexRegionGenerator : IRegionGenerator
    {
        private static readonly int HEX_SPACING = 12;
        private static readonly int HEX_VARIANCE = 2;

        public string RegionStyle => "mutated hex";

        public Dictionary<CellPosition, List<Cell>> FormRegions(Random random, Cell[,] cells)
        {
            var allCells = new List<Cell>();
            for (var x = 0; x < cells.GetLength(0); x++)
            {
                for (var y = 0; y < cells.GetLength(1); y++)
                {
                    allCells.Add(cells[x, y]);
                }
            }

            var worldSize = allCells.Max(c => c.Position.X);

            var planeId = allCells.First().Position.Plane;

            var hexesWide = (int)(Math.Ceiling(worldSize / (double)HEX_SPACING) * HEX_SPACING);

            // Place key points
            var keyPoints = new List<CellPosition>();
            var row = 0;
            for(var y = 0; y <  hexesWide; y += HEX_SPACING / 2)
            {
                var offset = row % 2 == 0 ? 0 : HEX_SPACING / 2;

                for(var x = offset; x < hexesWide; x += HEX_SPACING)
                {
                    keyPoints.Add(new CellPosition(
                        planeId, 
                        x + random.Next(-HEX_VARIANCE, HEX_VARIANCE + 1),
                        y + random.Next(-HEX_VARIANCE, HEX_VARIANCE + 1)));
                }

                row += 1;
            }

            keyPoints = keyPoints.Distinct().ToList();
            
            var tentativeRegions = MapToClosest(keyPoints, cells, allCells);
            
            foreach (var tentativeRegion in tentativeRegions)
            {
                if (!tentativeRegion.Value.Any())
                {
                    keyPoints.Remove(tentativeRegion.Key);
                }
            }
            tentativeRegions = MapToClosest(keyPoints, cells, allCells);

            return tentativeRegions;
        }

        private static Dictionary<CellPosition, List<Cell>> MapToClosest(List<CellPosition> keyPoints, Cell[,] cells, List<Cell> allCells)
        {
            var claims = allCells.ToDictionary(c => c.Position, c => new List<CellPosition>());

            var initialSweep = HEX_SPACING / 2;

            foreach (var keypoint in keyPoints)
            {
                for (var x = -initialSweep; x <= initialSweep; x++)
                {
                    for (var y = -initialSweep; y <= initialSweep; y++)
                    {
                        var toCheck = new CellPosition(keypoint.Plane, keypoint.X + x, keypoint.Y + y);

                        if (claims.ContainsKey(toCheck))
                        {
                            claims[toCheck].Add(keypoint);
                        }
                    }
                }
            }

            var tentativeRegions = keyPoints
                .ToDictionary(k => k, k => new List<Cell>());

            foreach (var claim in claims)
            {
                var candidates = claim.Value;

                if (!candidates.Any())
                {
                    candidates = keyPoints;
                }

                var owner = candidates.GetLowest(p => p.DistanceSq(claim.Key));

                tentativeRegions[owner].Add(cells[claim.Key.X, claim.Key.Y]);
            }

            return tentativeRegions;
        }
    }
}

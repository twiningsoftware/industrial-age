@echo off 

REM ************************************************************
REM 					CONFIGURATION
REM ************************************************************

REM Change this to match the name of your mod
SET modname=IndustrialAge

REM Change this to the install path of bronze age
REM SET bronzepath=%appdata%\itch\apps\bronze-age-beta\
 SET bronzepath=%appdata%\itch\apps\bronze-age\
REM SET bronzepath= ..\bronzeage\output\Debug\Windows\


REM ************************************************************
REM 					SETUP
REM ************************************************************

SET destination=%bronzepath%\mods\%modname%

REM ************************************************************
REM 					CLEANUP
REM ************************************************************

ECHO ***** Removing Old Version *****

RMDIR /Q /S %destination%

REM ************************************************************
REM 					DEPLOYING
REM ************************************************************

ECHO ***** Deploying New Version *****

MKDIR %destination%

ECHO Scripts
ROBOCOPY . %destination% *.cs /S /NJH /NJS /NDL
RMDIR /Q /S %destination%\obj

ECHO Data
ROBOCOPY . %destination% *.xml /S /NJH /NJS /NDL

ECHO Namelists
ROBOCOPY . %destination% *.csv /S /NJH /NJS /NDL

ECHO Images
ROBOCOPY . %destination% *.png /S /NJH /NJS /NDL

ECHO ***** Finished! *****